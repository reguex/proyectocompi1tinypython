//
// Created by Doninelli on 12/2/17.
//

#include <iostream>
#include "Parser.h"

using namespace std;
using namespace ast;
using namespace token;

parser::Parser::Parser(lexer::Lexer &lexer) : lexer(lexer), currentToken(nullptr)
{}

shared_ptr<Node> parser::Parser::buildAST()
{
    currentToken = lexer.scan();

    return statementList();
}

shared_ptr<Node> parser::Parser::statementList()
{
    auto token = currentToken;
    auto statements = make_shared<vector<shared_ptr<Node>>>(vector<shared_ptr<Node>>());

    statements->push_back(statement());
    while (currentToken->getType() != TokenType::END_OF_FILE &&
           currentToken->getType() != TokenType::DEDENT)
    {
        statements->push_back(statement());
    }

    if (currentToken->getType() == TokenType::DEDENT)
    {
        consume(TokenType::DEDENT);
    }

    return make_shared<StatementListNode>(StatementListNode(token, statements));
}

shared_ptr<Node> parser::Parser::statement()
{
    shared_ptr<Node> node;
    switch (currentToken->getType())
    {

        case TokenType::ID:
            node = assignmentStatement();
            break;

        case TokenType::WHILE:
            node = whileStatement();
            break;

        case TokenType::IF:
            node = ifStatement();
            break;

        case TokenType::FOR:
            node = forStatement();
            break;

        case TokenType::PASS:
            node = pass();
            break;

        case TokenType::INDENT:
            node = block();
            break;

        case TokenType::PRINT:
            node = printStatement();
            break;

        default:
            raiseUnexpectedTokenError();

    }

    return node;
}

shared_ptr<Node> parser::Parser::assignmentStatement()
{
    auto token = currentToken;
    auto varNode = lValue();
    consume(TokenType::ASSIGN);
    auto rValNode = rValue();
    return make_shared<AssignmentNode>(AssignmentNode(token, varNode, rValNode));
}

void parser::Parser::raiseUnexpectedTokenError()
{
    string message = "Unexpected Token ";
    message += stringify(currentToken->getType());
    message += " Fount At: ";
    message += to_string(currentToken->getLine());
    throw invalid_argument(message);
}

shared_ptr<Node> parser::Parser::variable()
{
    auto token = currentToken;
    auto varName = currentToken->getLexeme();
    consume(TokenType::ID);
    return make_shared<VarNode>(VarNode(token, varName));
}

void parser::Parser::consume(TokenType type)
{
    if (currentToken->getType() == type)
    {
        currentToken = lexer.scan();
    }
    else
    {
        raiseUnexpectedTokenError();
    }
}

shared_ptr<Node> parser::Parser::expression()
{
    auto token = currentToken;
    auto node = term();

    while (currentToken->getType() == TokenType::ADD || currentToken->getType() == TokenType::SUB)
    {
        if (currentToken->getType() == TokenType::ADD)
        {
            consume(TokenType::ADD);
            node = make_shared<AddBinaryOpNode>(AddBinaryOpNode(currentToken, node, term()));
        }
        else if (currentToken->getType() == TokenType::SUB)
        {
            consume(TokenType::SUB);
            node = make_shared<SubBinaryOpNode>(SubBinaryOpNode(currentToken, node, term()));
        }
    }

    return node;
}

shared_ptr<Node> parser::Parser::term()
{
    auto token = currentToken;
    auto node = power();

    while (currentToken->getType() == TokenType::MUL ||
           currentToken->getType() == TokenType::DIV ||
           currentToken->getType() == TokenType::MOD)
    {

        if (currentToken->getType() == TokenType::MUL)
        {
            consume(TokenType::MUL);
            node = make_shared<MulBinaryOpNode>(MulBinaryOpNode(token, node, power()));
        }
        else if (currentToken->getType() == TokenType::DIV)
        {
            consume(TokenType::DIV);
            node = make_shared<DivBinaryOpNode>(DivBinaryOpNode(token, node, power()));
        }
        else if (currentToken->getType() == TokenType::MOD)
        {
            consume(TokenType::MOD);
            node = make_shared<ModBinaryOpNode>(ModBinaryOpNode(token, node, power()));
        }
    }

    return node;
}

void parser::Parser::newLine()
{
    if (currentToken->getType() != TokenType::END_OF_FILE)
    {
        consume(TokenType::NEW_LINE);
    }
}

shared_ptr<Node> parser::Parser::factor()
{

    shared_ptr<Node> node;
    switch (currentToken->getType())
    {
        case TokenType::ID:
            node = lValue();
            break;

        case TokenType::INT:
            node = intLiteral();
            break;

        case TokenType::L_PAREN:
            consume(TokenType::L_PAREN);
            node = expression();
            consume(TokenType::R_PAREN);
            break;

        case TokenType::INPUT:
            node = input();
            break;

        case TokenType::LEN:
            node = lenExpression();
            break;

        default:
            raiseUnexpectedTokenError();

    }

    return node;
}

shared_ptr<Node> parser::Parser::power()
{
    auto token = currentToken;
    auto node = factor();

    while (currentToken->getType() == TokenType::EXP)
    {
        consume(TokenType::EXP);
        node = make_shared<PowBinaryOpNode>(PowBinaryOpNode(token, node, factor()));
    }

    return node;
}

shared_ptr<Node> parser::Parser::intLiteral()
{
    auto token = currentToken;
    auto value = currentToken->getLexeme();
    consume(TokenType::INT);
    auto intValue = atoi(value.c_str()); // NOLINT
    return make_shared<IntLiteralNode>(IntLiteralNode(token, intValue));
}

shared_ptr<Node> parser::Parser::stringLiteral()
{
    auto token = currentToken;
    auto value = currentToken->getLexeme();
    consume(TokenType::STRING);
    value.erase(0, 1);
    value.erase(value.size() - 1);
    return make_shared<StringLiteralNode>(StringLiteralNode(token, value));
}

shared_ptr<Node> parser::Parser::whileStatement()
{
    auto token = currentToken;
    consume(TokenType::WHILE);
    auto conditionNode = booleanExpression();
    consume(TokenType::COLON);
    auto bodyNode = block();
    return make_shared<WhileNode>(WhileNode(token, conditionNode, bodyNode));
}

shared_ptr<Node> parser::Parser::booleanExpression()
{
    auto token = currentToken;
    shared_ptr<Node> node = expression();

    switch (currentToken->getType())
    {
        case TokenType::EQUAL:
            consume(TokenType::EQUAL);
            node = make_shared<EqualNode>(EqualNode(token, node, expression()));
            break;

        case TokenType::NOT_EQUALS:
            consume(TokenType::NOT_EQUALS);
            node = make_shared<NotEqualNode>(NotEqualNode(token, node, expression()));

        case TokenType::LESS_THAN:
            consume(TokenType::LESS_THAN);
            node = make_shared<LessThanNode>(LessThanNode(token, node, expression()));
            break;

        case TokenType::GREATER_THAN:
            consume(TokenType::GREATER_THAN);
            node = make_shared<GreaterThanNode>(GreaterThanNode(token, node, expression()));
            break;

        case TokenType::LESS_THAN_OR_EQ:
            consume(TokenType::LESS_THAN_OR_EQ);
            node = make_shared<LessThanOrEqualNode>(LessThanOrEqualNode(token, node, expression()));
            break;

        case TokenType::GREATER_THAN_OR_EQ:
            consume(TokenType::GREATER_THAN_OR_EQ);
            node = make_shared<GreaterThanOrEqualNode>(GreaterThanOrEqualNode(token, node, expression()));
            break;

        default:
            break;

    }

    return node;
}

shared_ptr<Node> parser::Parser::block()
{
    auto token = currentToken;
    consume(TokenType::INDENT);
    auto statements = statementList();
    return make_shared<BlockNode>(BlockNode(token, statements));
}

shared_ptr<Node> parser::Parser::ifStatement()
{
    auto token = currentToken;
    consume(TokenType::IF);
    auto conditionNode = booleanExpression();
    consume(TokenType::COLON);
    auto ifNode = block();
    shared_ptr<Node> elseNode = nullptr;
    if (currentToken->getType() == TokenType::ELSE)
    {
        consume(TokenType::ELSE);
        consume(TokenType::COLON);
        elseNode = block();
    }
    else
    {
        elseNode = make_shared<NoOpNode>(NoOpNode(token));
    }

    return make_shared<IfNode>(IfNode(token, conditionNode, ifNode, elseNode));
}

shared_ptr<Node> parser::Parser::forStatement()
{
    auto token = currentToken;
    consume(TokenType::FOR);
    auto varNode = variable();
    consume(TokenType::IN);
    shared_ptr<Node> rangeNode;
    if (currentToken->getType() == TokenType::L_BRACKET)
    {
        rangeNode = array();
    }
    else
    {
        rangeNode = rangeExpression();
    }

    consume(TokenType::COLON);
    auto body = block();

    return make_shared<ForNode>(ForNode(token, varNode, rangeNode, body));
}

shared_ptr<Node> parser::Parser::rangeExpression()
{
    auto token = currentToken;
    consume(TokenType::RANGE);
    consume(TokenType::L_PAREN);
    auto lowerNode = expression();
    consume(TokenType::COMMA);
    auto upperNode = expression();
    consume(TokenType::R_PAREN);

    return make_shared<RangeNode>(RangeNode(token, lowerNode, upperNode));
}

shared_ptr<Node> parser::Parser::rValue()
{

    shared_ptr<Node> node = nullptr;
    if (currentToken->getType() == TokenType::STRING)
    {
        node = stringLiteral();
    }
    else if (currentToken->getType() == TokenType::L_BRACKET)
    {
        node = array();
    }
    else
    {
        node = booleanExpression();
    }

    return node;
}

shared_ptr<Node> parser::Parser::pass()
{
    auto token = currentToken;
    consume(TokenType::PASS);
    return make_shared<NoOpNode>(NoOpNode(currentToken));
}

shared_ptr<Node> parser::Parser::input()
{
    auto token = currentToken;
    consume(TokenType::INPUT);
    consume(TokenType::L_PAREN);
    auto node = stringLiteral();
    consume(TokenType::R_PAREN);
    return make_shared<InputNode>(InputNode(token, node));
}

shared_ptr<Node> parser::Parser::printStatement()
{
    auto token = currentToken;
    consume(TokenType::PRINT);
    consume(TokenType::L_PAREN);
    auto argListNode = printArgList();
    consume(TokenType::R_PAREN);
    return make_shared<PrintNode>(PrintNode(token, argListNode));
}

shared_ptr<Node> parser::Parser::printArgList()
{
    auto token = currentToken;
    shared_ptr<Node> node = nullptr;
    if (currentToken->getType() != TokenType::R_PAREN)
    {
        auto nodes = makeNodeVector();
        nodes->push_back(rValue());
        while (currentToken->getType() == TokenType::COMMA)
        {
            consume(TokenType::COMMA);
            nodes->push_back(rValue());
        }
        node = make_shared<PrintArgListNode>(PrintArgListNode(token, nodes));
    }
    else
    {
        node = make_shared<NoOpNode>(NoOpNode(token));
    }
    return node;
}

shared_ptr<Node> parser::Parser::array()
{
    auto token = currentToken;
    consume(TokenType::L_BRACKET);

    auto nodes = makeNodeVector();
    nodes->push_back(expression());

    while (currentToken->getType() == TokenType::COMMA)
    {
        consume(TokenType::COMMA);
        nodes->push_back(expression());
    }

    consume(TokenType::R_BRACKET);

    return make_shared<ArrayNode>(ArrayNode(token, nodes));
}

shared_ptr<vector<shared_ptr<Node>>> parser::Parser::makeNodeVector()
{
    return make_shared<vector<shared_ptr<Node>>>(vector<shared_ptr<Node>>());
}

shared_ptr<Node> parser::Parser::lenExpression()
{
    auto token = currentToken;
    consume(TokenType::LEN);
    consume(TokenType::L_PAREN);
    auto contentNode = variable();
    consume(TokenType::R_PAREN);

    return make_shared<LenNode>(LenNode(token, contentNode));
}

shared_ptr<Node> parser::Parser::lValue()
{
    auto token = currentToken;
    auto node = variable();
    if (currentToken->getType() == TokenType::L_BRACKET)
    {
        consume(TokenType::L_BRACKET);
        node = make_shared<IndexSetNode>(IndexSetNode(token, node, expression()));
        consume(TokenType::R_BRACKET);
    }
    return node;
}


