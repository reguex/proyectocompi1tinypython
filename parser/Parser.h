//
// Created by Doninelli on 12/2/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_PARSER_H
#define PROYECTOCOMPI1TINYPYTHON_PARSER_H


#include <memory>

#include "../lexer/Lexer.h"
#include "../ast/Node.h"

namespace parser
{
    class Parser
    {
        lexer::Lexer &lexer;
        std::shared_ptr<token::Token> currentToken;

    public:
        explicit Parser(lexer::Lexer &lexer);

        std::shared_ptr<ast::Node> buildAST();

    private:
        std::shared_ptr<ast::Node> statementList();

        std::shared_ptr<ast::Node> statement();

        std::shared_ptr<ast::Node> assignmentStatement();

        [[noreturn]] void raiseUnexpectedTokenError();

        std::shared_ptr<ast::Node> variable();

        void consume(token::TokenType type);

        std::shared_ptr<ast::Node> expression();

        void newLine();

        std::shared_ptr<ast::Node> term();

        std::shared_ptr<ast::Node> factor();

        std::shared_ptr<ast::Node> power();

        std::shared_ptr<ast::Node> intLiteral();

        std::shared_ptr<ast::Node> stringLiteral();

        std::shared_ptr<ast::Node> whileStatement();

        std::shared_ptr<ast::Node> booleanExpression();

        std::shared_ptr<ast::Node> block();

        std::shared_ptr<ast::Node> ifStatement();

        std::shared_ptr<ast::Node> forStatement();

        std::shared_ptr<ast::Node> rangeExpression();

        std::shared_ptr<ast::Node> rValue();

        std::shared_ptr<ast::Node> pass();

        std::shared_ptr<ast::Node> input();

        std::shared_ptr<ast::Node> printStatement();

        std::shared_ptr<ast::Node> printArgList();

        std::shared_ptr<ast::Node> array();

        std::shared_ptr<std::vector<std::shared_ptr<ast::Node>>> makeNodeVector();

        std::shared_ptr<ast::Node> lenExpression();

        std::shared_ptr<ast::Node> lValue();
    };
}


#endif //PROYECTOCOMPI1TINYPYTHON_PARSER_H
