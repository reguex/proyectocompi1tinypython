# ******************************************
#  * File: ForTest.py
#  * A test program for the 'for' statement
#  ******************************************

x = [1,2,3]
for i in range(0, len(x)):
    print ('i = ', i)

print ('End of loop')
