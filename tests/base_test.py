
arr = [5, 3, 23, 6, 12, 9, 7]
for i in range(0, len(arr)):
    print("i: ", i)
    for j in range(0, len(arr)-i-1):
        print("j: ", j)
        if arr[j] > arr[j+1]:
            temp = arr[j]
            arr[j] = arr[j+1]
            arr[j+1] = temp
print(arr)
