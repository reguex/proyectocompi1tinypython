//
// Created by Doninelli on 12/2/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_TOKENTYPE_H
#define PROYECTOCOMPI1TINYPYTHON_TOKENTYPE_H


#include <string>

namespace token
{
    enum class TokenType
    {
        END_OF_FILE,
        NEW_LINE,           // \n
        SPACE,              // [ \t]

        INT,                // 12
        ID,                 // x
        STRING,             // "string"

        ADD,                // +
        SUB,                // -
        MUL,                // *
        DIV,                // /
        EXP,                // **
        MOD,                // %

        EQUAL,              // ==
        NOT_EQUALS,         // !=

        ASSIGN,             // =

        L_PAREN,            // (
        R_PAREN,            // )

        L_BRACKET,          // [
        R_BRACKET,          // ]

        INDENT,
        DEDENT,

        COMMA,              // ,
        COLON,              // :

        WHILE,              // while
        FOR,                // for,
        INPUT,              // input,
        PRINT,              // print,
        IF,                 // if
        ELSE,               // else,
        RANGE,              // range
        IN,                 // in
        PASS,               // pass
        LEN,                // len

        LESS_THAN,          // <
        LESS_THAN_OR_EQ,    // <=
        GREATER_THAN,       // >
        GREATER_THAN_OR_EQ  // >=
    };


    inline static std::string stringify(TokenType tokenType)
    {
        switch (tokenType)
        {
            case TokenType::STRING :
                return "STRING";

            case TokenType::INPUT:
                return "INPUT";

            case TokenType::ELSE:
                return "ELSE";

            case TokenType::COMMA:
                return "COMMA";

            case TokenType::DEDENT:
                return "DEDENT";

            case TokenType::INDENT:
                return "INDENT";

            case TokenType::RANGE:
                return "RANGE";

            case TokenType::FOR:
                return "FOR";

            case TokenType::END_OF_FILE:
                return "EOF";

            case TokenType::ID:
                return "ID";

            case TokenType::MOD:
                return "MOD";

            case TokenType::ADD:
                return "ADD";

            case TokenType::DIV:
                return "DIV";

            case TokenType::ASSIGN:
                return "ASSIGN";

            case TokenType::EQUAL:
                return "EQUAL";

            case TokenType::EXP:
                return "EXP";

            case TokenType::INT:
                return "INT";

            case TokenType::SUB:
                return "SUB";

            case TokenType::MUL:
                return "MUL";

            case TokenType::L_PAREN:
                return "L_PAREN";

            case TokenType::R_PAREN:
                return "R_PAREN";

            case TokenType::WHILE:
                return "WHILE";

            case TokenType::PRINT:
                return "PRINT";

            case TokenType::IF:
                return "IF";

            case TokenType::LESS_THAN:
                return "LESS_THAN";

            case TokenType::LESS_THAN_OR_EQ:
                return "GREATER_THAN_OR_EQUAL";

            case TokenType::GREATER_THAN:
                return "GREATER_THAN";

            case TokenType::GREATER_THAN_OR_EQ:
                return "GREATER_THAN_OR_EQUAL";

            case TokenType::NEW_LINE:
                return "NEW_LINE";

            case TokenType::SPACE:
                return "SPACE";

            case TokenType::COLON:
                return "COLON";

            case TokenType::IN:
                return "IN";

            case TokenType::PASS:
                return "PASS";

            case TokenType::L_BRACKET:
                return "L_BRACKET";

            case TokenType::R_BRACKET:
                return "R_BRACKET";

            case TokenType::LEN:
                return "LEN";
                
            case TokenType::NOT_EQUALS:
                return "NOT_EQUALS";
        }
    }
}


#endif //PROYECTOCOMPI1TINYPYTHON_TOKENTYPE_H
