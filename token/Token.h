//
// Created by Doninelli on 12/2/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_TOKEN_H
#define PROYECTOCOMPI1TINYPYTHON_TOKEN_H


#include <string>
#include <ostream>
#include "TokenType.h"
#include <memory>

namespace token
{
    class Token
    {
        std::string lexeme;
        TokenType type;
        int line;

    public:
        Token(const std::string &lexeme, TokenType type, int line);

        const std::string &getLexeme() const;

        TokenType getType() const;

        int getLine() const;

        friend std::ostream &operator<<(std::ostream &os, const Token &token)
        {
            os << "lexeme: '" << token.getLexeme() << "'" <<
               ", type: " << stringify(token.getType()) <<
               ", line: " << token.getLine();
            return os;
        }
    };

    inline std::shared_ptr<Token> newShared(const std::string &lexeme, TokenType type, int line)
    {
        return std::make_shared<Token>(Token(lexeme, type, line));
    }
}


#endif //PROYECTOCOMPI1TINYPYTHON_TOKEN_H
