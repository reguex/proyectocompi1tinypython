//
// Created by Doninelli on 12/2/17.
//

#include "Token.h"

token::Token::Token(const std::string &lexeme, token::TokenType type, int line) :
        lexeme(lexeme), type(type), line(line)
{}

const std::string &token::Token::getLexeme() const
{
    return lexeme;
}

token::TokenType token::Token::getType() const
{
    return type;
}

int token::Token::getLine() const
{
    return line;
}
