//
// Created by Edwin Herrera on 12/4/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_VARIABLETABLE_H
#define PROYECTOCOMPI1TINYPYTHON_VARIABLETABLE_H


#include <map>
#include "ValueType.h"

namespace ast {
    class VariableTable
    {
        std::shared_ptr<std::map<std::string, std::shared_ptr<ValueType>>> variables;

    public:
        VariableTable();

        void set(std::string key, std::shared_ptr<ValueType> value);

        std::shared_ptr<ValueType> find(std::string var);
    };

    extern std::shared_ptr<VariableTable> table;
}


#endif //PROYECTOCOMPI1TINYPYTHON_VARIABLETABLE_H
