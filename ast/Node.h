//
// Created by Doninelli on 12/2/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_NODE_H
#define PROYECTOCOMPI1TINYPYTHON_NODE_H


#include <string>
#include <memory>
#include <vector>
#include "ValueType.h"
#include "../token/Token.h"

namespace ast
{

    class Node
    {
        std::shared_ptr<token::Token> token;
    protected:
        [[noreturn]] inline void typeError(const std::string &expected)
        {
            std::string error = "Invalid Type. expected: " + expected + " Line " + std::to_string(token->getLine());
            throw std::invalid_argument(error);
        }

    public:
        explicit Node(const std::shared_ptr<token::Token> &token);

        virtual std::string toString() = 0;

        virtual std::shared_ptr<ValueType> getValue();

        const std::shared_ptr<token::Token> &getToken() const;
    };

    class VarNode : public Node
    {
        std::string varName;

    public:
        explicit VarNode(std::shared_ptr<token::Token> token, std::string varName);

        std::string toString() override;

        const std::string &getVarName() const;

        std::shared_ptr<ValueType> getValue() override;
    };

    template<typename T>
    class LiteralNode : public Node
    {
    protected:
        T value;

    public:
        explicit LiteralNode(std::shared_ptr<token::Token> token, T value) : Node(token), value(value)
        {}

        virtual std::string toString() = 0; // NOLINT
    };

    class IntLiteralNode : public LiteralNode<int>
    {
    public:
        explicit IntLiteralNode(std::shared_ptr<token::Token> token, int value);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class StringLiteralNode : public LiteralNode<std::string>
    {
    public:
        explicit StringLiteralNode(std::shared_ptr<token::Token> token, std::string value);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class BinaryOpNode : public Node
    {

    protected:
        std::shared_ptr<ast::Node> left;
        std::shared_ptr<ast::Node> right;
    public:
        BinaryOpNode(std::shared_ptr<token::Token> token,
                     const std::shared_ptr<Node> &left,
                     const std::shared_ptr<Node> &right);

        const std::shared_ptr<Node> &getLeft() const;

        const std::shared_ptr<Node> &getRight() const;

        virtual std::string toString() = 0; // NOLINT
    };

    class AddBinaryOpNode : public BinaryOpNode
    {
    public:
        AddBinaryOpNode(std::shared_ptr<token::Token> token,
                        const std::shared_ptr<Node> &left,
                        const std::shared_ptr<Node> &right);

        std::shared_ptr<ValueType> getValue() override;

        std::string toString() override;
    };

    class SubBinaryOpNode : public BinaryOpNode
    {
    public:
        SubBinaryOpNode(std::shared_ptr<token::Token> token,
                        const std::shared_ptr<Node> &left,
                        const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class DivBinaryOpNode : public BinaryOpNode
    {
    public:
        DivBinaryOpNode(std::shared_ptr<token::Token> token,
                        const std::shared_ptr<Node> &left,
                        const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class MulBinaryOpNode : public BinaryOpNode
    {
    public:
        MulBinaryOpNode(std::shared_ptr<token::Token>,
                        const std::shared_ptr<Node> &left,
                        const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class ModBinaryOpNode : public BinaryOpNode
    {
    public:
        ModBinaryOpNode(std::shared_ptr<token::Token> token,
                        const std::shared_ptr<Node> &left,
                        const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;

    };

    class PowBinaryOpNode : public BinaryOpNode
    {
    public:
        PowBinaryOpNode(std::shared_ptr<token::Token> token,
                        const std::shared_ptr<Node> &left,
                        const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class InputNode : public Node
    {
        std::shared_ptr<ast::Node> node;
    public:
        explicit InputNode(std::shared_ptr<token::Token> token, const std::shared_ptr<Node> &node);

        const std::shared_ptr<Node> &getNode() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class FactorNode : public Node
    {
        std::shared_ptr<ast::Node> node;
    public:
        explicit FactorNode(std::shared_ptr<token::Token> token, const std::shared_ptr<Node> &node);

        const std::shared_ptr<Node> &getNode() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class PowerNode : public Node
    {
        std::shared_ptr<ast::Node> node;
    public:
        explicit PowerNode(std::shared_ptr<token::Token> token, const std::shared_ptr<Node> &node);

        const std::shared_ptr<Node> &getNode() const;

        std::string toString() override;
    };

    class NoOpNode : public Node
    {
    public:
        explicit NoOpNode(const std::shared_ptr<token::Token> &token);

        std::string toString() override;
    };

    class EqualNode : public BinaryOpNode
    {
    public:
        EqualNode(std::shared_ptr<token::Token> token,
                  const std::shared_ptr<Node> &left,
                  const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class NotEqualNode : public BinaryOpNode
    {
    public:
        NotEqualNode(std::shared_ptr<token::Token> token,
                     const std::shared_ptr<Node> &left,
                     const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class LessThanNode : public BinaryOpNode
    {
    public:
        LessThanNode(std::shared_ptr<token::Token> token,
                     const std::shared_ptr<Node> &left,
                     const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class GreaterThanNode : public BinaryOpNode
    {
    public:
        GreaterThanNode(std::shared_ptr<token::Token> token,
                        const std::shared_ptr<Node> &left,
                        const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class LessThanOrEqualNode : public BinaryOpNode
    {
    public:
        LessThanOrEqualNode(std::shared_ptr<token::Token> token,
                            const std::shared_ptr<Node> &left,
                            const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class GreaterThanOrEqualNode : public BinaryOpNode
    {
    public:
        GreaterThanOrEqualNode(std::shared_ptr<token::Token> token,
                               const std::shared_ptr<Node> &left,
                               const std::shared_ptr<Node> &right);

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class AssignmentNode : public Node
    {
        std::shared_ptr<ast::Node> varNode;
        std::shared_ptr<ast::Node> rValueNode;

    public:
        AssignmentNode(std::shared_ptr<token::Token> token,
                       const std::shared_ptr<Node> &varNode,
                       const std::shared_ptr<Node> &rValueNode);

        const std::shared_ptr<Node> &getVarNode() const;

        const std::shared_ptr<Node> &getRValueNode() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class WhileNode : public Node
    {
        std::shared_ptr<ast::Node> conditionNode;
        std::shared_ptr<ast::Node> bodyNode;

    public:
        WhileNode(std::shared_ptr<token::Token> token,
                  const std::shared_ptr<Node> &conditionNode,
                  const std::shared_ptr<Node> &bodyNode);

        const std::shared_ptr<Node> &getConditionNode() const;

        const std::shared_ptr<Node> &getBodyNode() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class BlockNode : public Node
    {
        std::shared_ptr<Node> statementListNode;

    public:
        explicit BlockNode(std::shared_ptr<token::Token> token, const std::shared_ptr<Node> &statementListNode);

        std::string toString() override;

        const std::shared_ptr<Node> &getStatementListNode() const;

        std::shared_ptr<ValueType> getValue() override;
    };

    class StatementListNode : public Node
    {
        std::shared_ptr<std::vector<std::shared_ptr<Node>>> statements;

    public:
        explicit StatementListNode(std::shared_ptr<token::Token> token,
                                   const std::shared_ptr<std::vector<std::shared_ptr<Node>>> &statements);

        std::string toString() override;

        const std::shared_ptr<std::vector<std::shared_ptr<Node>>> &getStatements() const;

        std::shared_ptr<ValueType> getValue() override;
    };

    class PrintArgListNode : public Node
    {
        std::shared_ptr<std::vector<std::shared_ptr<Node>>> args;
    public:
        explicit PrintArgListNode(std::shared_ptr<token::Token> token,
                                  const std::shared_ptr<std::vector<std::shared_ptr<Node>>> &args);

        const std::shared_ptr<std::vector<std::shared_ptr<Node>>> &getArgs() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class PrintNode : public Node
    {
        std::shared_ptr<Node> node;

    public:
        explicit PrintNode(std::shared_ptr<token::Token> token, const std::shared_ptr<Node> &node);

        const std::shared_ptr<Node> &getNode() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class IfNode : public Node
    {
        std::shared_ptr<Node> conditionNode;
        std::shared_ptr<Node> ifBodyNode;
        std::shared_ptr<Node> elseBodyNode;

    public:
        IfNode(std::shared_ptr<token::Token> token,
               const std::shared_ptr<Node> &conditionNode,
               const std::shared_ptr<Node> &ifBodyNode,
               const std::shared_ptr<Node> &elseBodyNode);

        const std::shared_ptr<Node> &getConditionNode() const;

        const std::shared_ptr<Node> &getIfBodyNode() const;

        const std::shared_ptr<Node> &getElseBodyNode() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class ForNode : public Node
    {
        std::shared_ptr<Node> varNode;
        std::shared_ptr<Node> rangeNode;
        std::shared_ptr<Node> body;

    public:
        ForNode(std::shared_ptr<token::Token> token,
                const std::shared_ptr<Node> &varNode,
                const std::shared_ptr<Node> &rangeNode,
                const std::shared_ptr<Node> &body);

        const std::shared_ptr<Node> &getVarNode() const;

        const std::shared_ptr<Node> &getRangeNode() const;

        const std::shared_ptr<Node> &getBody() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class RangeNode : public Node
    {
        std::shared_ptr<ast::Node> lowerBoundNode;
        std::shared_ptr<ast::Node> upperBoundNode;

    public:
        RangeNode(std::shared_ptr<token::Token> token,
                  const std::shared_ptr<Node> &lowerBoundNode,
                  const std::shared_ptr<Node> &upperBoundNode);

        const std::shared_ptr<Node> &getLowerBoundNode() const;

        const std::shared_ptr<Node> &getUpperBoundNode() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class ArrayNode : public Node
    {
        std::shared_ptr<std::vector<std::shared_ptr<Node>>> elems;

    public:
        explicit ArrayNode(std::shared_ptr<token::Token> token,
                           const std::shared_ptr<std::vector<std::shared_ptr<Node>>> &elems);

        const std::shared_ptr<std::vector<std::shared_ptr<Node>>> &getElems() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class LenNode : public Node
    {
        std::shared_ptr<Node> contentNode;

    public:
        explicit LenNode(std::shared_ptr<token::Token> token, const std::shared_ptr<Node> &contentNode);

        const std::shared_ptr<Node> &getContentNode() const;

        std::string toString() override;

        std::shared_ptr<ValueType> getValue() override;
    };

    class IndexingNode : public Node
    {
        std::shared_ptr<Node> varNode;
        std::shared_ptr<Node> indexNode;

    public:
        IndexingNode(std::shared_ptr<token::Token> token,
                     const std::shared_ptr<Node> &varNode,
                     const std::shared_ptr<Node> &indexNode);

        const std::shared_ptr<Node> &getVarNode() const;

        const std::shared_ptr<Node> &getIndexNode() const;

        std::string toString() override;
    };

    class IndexAccessNode : public IndexingNode
    {
    public:
        IndexAccessNode(std::shared_ptr<token::Token> token,
                        const std::shared_ptr<Node> &varNode,
                        const std::shared_ptr<Node> &indexNode);
    };

    class IndexSetNode : public IndexingNode
    {
    public:
        IndexSetNode(std::shared_ptr<token::Token> token,
                     const std::shared_ptr<Node> &varNode,
                     const std::shared_ptr<Node> &indexNode);

        std::shared_ptr<ValueType> getValue() override;

    };
}


#endif //PROYECTOCOMPI1TINYPYTHON_NODE_H
