//
// Created by Edwin Herrera on 12/4/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_VALUETYPE_H
#define PROYECTOCOMPI1TINYPYTHON_VALUETYPE_H


#include <string>
#include <memory>
#include <iostream>
#include <vector>

namespace ast
{
    class ValueType
    {
    public:
        virtual ~ValueType();

        virtual std::string toString() = 0;
    };

    class IntValueType : public ValueType
    {
        int value;

    public:
        explicit IntValueType(int value);

        int getValue() const;

        std::string toString() override;
    };

    class StringValueType : public ValueType
    {
        std::string value;

    public:
        explicit StringValueType(const std::string &value);

        const std::string &getValue() const;

        std::string toString() override;
    };

    class NoValue : public ValueType
    {
    public:
        std::string toString() override;
    };

    class BoolValueType : public ValueType
    {
        bool value;

    public:
        explicit BoolValueType(bool value);

        bool getValue() const;

        std::string toString() override;
    };

    class ArrayValueType : public ValueType
    {
        std::shared_ptr<std::vector<std::shared_ptr<ValueType>>> values;

    public:
        explicit ArrayValueType(const std::shared_ptr<std::vector<std::shared_ptr<ValueType>>> &values);

        const std::shared_ptr<std::vector<std::shared_ptr<ValueType>>> &getValues() const;

        std::string toString() override;
    };
}


#endif //PROYECTOCOMPI1TINYPYTHON_VALUETYPE_H
