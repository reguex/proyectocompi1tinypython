//
// Created by Edwin Herrera on 12/4/17.
//

#include "VariableTable.h"


using namespace std;
using namespace ast;


auto ast::table = make_shared<VariableTable>(VariableTable());

VariableTable::VariableTable()
{
    variables = make_shared<map<string, shared_ptr<ValueType>>>(map<string, shared_ptr<ValueType>>());
};


void VariableTable::set(string key, shared_ptr<ValueType> value)
{
    if (variables->find(key) != variables->end())
    {
        variables->at(key) = value;
    }
    else
    {
        variables->insert(pair<string, shared_ptr<ValueType>>(key, value));
    }
}

shared_ptr<ValueType> VariableTable::find(string var)
{
    if (variables->find(var) != variables->end())
    {
        return variables->at(var);
    }
    else
    {
        return make_shared<NoValue>(NoValue());
    }
}
