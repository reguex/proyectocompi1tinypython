//
// Created by Edwin Herrera on 12/4/17.
//

#include "ValueType.h"

ast::IntValueType::IntValueType(int value) : value(value)
{}

int ast::IntValueType::getValue() const
{
    return value;
}

std::string ast::IntValueType::toString()
{
    return std::to_string(value);
}

ast::StringValueType::StringValueType(const std::string &value) : value(value)
{}

const std::string &ast::StringValueType::getValue() const
{
    return value;
}

std::string ast::StringValueType::toString()
{
    return value;
}

ast::ValueType::~ValueType() = default;

ast::BoolValueType::BoolValueType(bool value) : value(value)
{}

bool ast::BoolValueType::getValue() const
{
    return value;
}

std::string ast::BoolValueType::toString()
{
    return value ? "True" : "False";
}

std::string ast::NoValue::toString()
{
    return "";
}

ast::ArrayValueType::ArrayValueType(const std::shared_ptr<std::vector<std::shared_ptr<ast::ValueType>>> &values)
        : values(values)
{}

const std::shared_ptr<std::vector<std::shared_ptr<ast::ValueType>>> &ast::ArrayValueType::getValues() const
{
    return values;
}

std::string ast::ArrayValueType::toString()
{
    std::string string;
    for (const auto &elem : *values)
    {
        if (!string.empty())
        {
            string += ", ";
        }
        string += elem->toString();
    }

    return "[" + string + "]";
}
