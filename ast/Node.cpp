//
// Created by Doninelli on 12/2/17.
//

#include <cmath>
#include "Node.h"
#include "VariableTable.h"

using namespace std;
using namespace ast;
using namespace token;

VarNode::VarNode(shared_ptr<Token> token, string varName) : Node(token), varName(varName)
{}

string VarNode::toString()
{
    return "VarNode(" + varName + ")";
}

const string &VarNode::getVarName() const
{
    return varName;
}

shared_ptr<ValueType> VarNode::getValue()
{
    return table->find(varName);
}

IntLiteralNode::IntLiteralNode(shared_ptr<Token> token, int value) : LiteralNode(token, value)
{}

string IntLiteralNode::toString()
{
    return "IntLiteralNode(value: " + to_string(value) + ")";
}

shared_ptr<ValueType> IntLiteralNode::getValue()
{
    return make_shared<IntValueType>(IntValueType(value));
}

StringLiteralNode::StringLiteralNode(shared_ptr<Token> token, string value) : LiteralNode(token, move(value))
{}

string StringLiteralNode::toString()
{
    return "StringLiteralNode(value: " + value + ")";
}

shared_ptr<ValueType> StringLiteralNode::getValue()
{
    return make_shared<StringValueType>(StringValueType(value));
};

BinaryOpNode::BinaryOpNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right) :
        Node(token), left(left), right(right)
{}

const shared_ptr<Node> &BinaryOpNode::getLeft() const
{
    return left;
}

const shared_ptr<Node> &BinaryOpNode::getRight() const
{
    return right;
}

AddBinaryOpNode::AddBinaryOpNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string AddBinaryOpNode::toString()
{
    return "AddBinaryOpNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> AddBinaryOpNode::getValue()
{
    auto lValue = left->getValue();
    auto rValue = right->getValue();
    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(lValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rValue))
        {
            return make_shared<IntValueType>(IntValueType(l->getValue() + r->getValue()));
        }
        else
        {
            typeError("int");
        }
    }
    else if (auto l = dynamic_pointer_cast<StringValueType>(lValue))
    {
        if (auto _ = dynamic_pointer_cast<NoValue>(rValue))
        {
            typeError("string, int, bool, array");
        }
        else
        {
            return make_shared<StringValueType>(StringValueType(l->getValue() + rValue->toString()));
        }
    }

    typeError("int, string");
}

SubBinaryOpNode::SubBinaryOpNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string SubBinaryOpNode::toString()
{
    return "SubBinaryOpNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> SubBinaryOpNode::getValue()
{
    auto lValue = left->getValue();
    auto rValue = right->getValue();
    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(lValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rValue))
        {
            return make_shared<IntValueType>(IntValueType(l->getValue() - r->getValue()));
        }
    }

    typeError("int");
}

DivBinaryOpNode::DivBinaryOpNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string DivBinaryOpNode::toString()
{
    return "DivBinaryOpNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> DivBinaryOpNode::getValue()
{
    auto lValue = left->getValue();
    auto rValue = right->getValue();
    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(lValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rValue))
        {
            return make_shared<IntValueType>(IntValueType(l->getValue() / r->getValue()));
        }
    }

    typeError("int");
}

MulBinaryOpNode::MulBinaryOpNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string MulBinaryOpNode::toString()
{
    return "MulBinaryOpNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> MulBinaryOpNode::getValue()
{
    auto lValue = left->getValue();
    auto rValue = right->getValue();
    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(lValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rValue))
        {
            return make_shared<IntValueType>(IntValueType(l->getValue() * r->getValue()));
        }
    }

    typeError("int");
}

ModBinaryOpNode::ModBinaryOpNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string ModBinaryOpNode::toString()
{
    return "ModBinaryOpNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> ModBinaryOpNode::getValue()
{
    auto lValue = left->getValue();
    auto rValue = right->getValue();
    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(lValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rValue))
        {
            return make_shared<IntValueType>(IntValueType(l->getValue() % r->getValue()));
        }
    }

    typeError("int");
}

PowBinaryOpNode::PowBinaryOpNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string PowBinaryOpNode::toString()
{
    return "PowBinaryOpNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> PowBinaryOpNode::getValue()
{
    auto lValue = left->getValue();
    auto rValue = right->getValue();
    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(lValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rValue))
        {
            return make_shared<IntValueType>(IntValueType(
                    static_cast<int>(pow(l->getValue(), r->getValue()))));
        }
    }

    typeError("int");
}

InputNode::InputNode(shared_ptr<Token> token, const shared_ptr<Node> &node) : Node(token), node(node)
{}

const shared_ptr<Node> &InputNode::getNode() const
{
    return node;
}

string InputNode::toString()
{
    return "InputNode(message: " + node->toString() + ")";
}

shared_ptr<ValueType> InputNode::getValue()
{
    if (auto promptValue = dynamic_pointer_cast<StringValueType>(node->getValue()))
    {
        auto promptMessage = promptValue->getValue();
        cout << promptMessage;
        string response;
        cin >> response;

        auto intValue = atoi(response.c_str()); // NOLINT
        return make_shared<IntValueType>(IntValueType(intValue));
    }
    else
    {
        typeError("string");
    }
}

FactorNode::FactorNode(shared_ptr<Token> token, const shared_ptr<Node> &node) : Node(token), node(node)
{}

const shared_ptr<Node> &FactorNode::getNode() const
{
    return node;
}

string FactorNode::toString()
{
    return "FactorNode(value: " + node->toString() + ")";
}

shared_ptr<ValueType> FactorNode::getValue()
{
    auto value = node->getValue();
    if (shared_ptr<NoValue> noValue = dynamic_pointer_cast<NoValue>(value))
    {
        typeError("");
    }

    return value;
}

PowerNode::PowerNode(shared_ptr<Token> token, const shared_ptr<Node> &node) : Node(token), node(node)
{}

const shared_ptr<Node> &PowerNode::getNode() const
{
    return node;
}

string PowerNode::toString()
{
    return "PowerNode(value:" + node->toString() + ")";
}

string NoOpNode::toString()
{
    return "NoOpNode()";
}

NoOpNode::NoOpNode(const shared_ptr<Token> &token) : Node(token)
{}

EqualNode::EqualNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string EqualNode::toString()
{
    return "EqualNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> EqualNode::getValue()
{
    auto leftValue = left->getValue();
    auto rightValue = right->getValue();

    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(leftValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() == r->getValue()));
        }
        else
        {
            typeError("int");
        }
    }
    else if (shared_ptr<BoolValueType> l = dynamic_pointer_cast<BoolValueType>(leftValue))
    {
        if (shared_ptr<BoolValueType> r = dynamic_pointer_cast<BoolValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() == r->getValue()));
        }
        else
        {
            typeError("bool");
        }
    }
    else if (shared_ptr<StringValueType> l = dynamic_pointer_cast<StringValueType>(leftValue))
    {
        if (shared_ptr<StringValueType> r = dynamic_pointer_cast<StringValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() == r->getValue()));
        }
        else
        {
            typeError("string");
        }
    }

    typeError("int, string, bool");
}

LessThanNode::LessThanNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string LessThanNode::toString()
{
    return "LessThanNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> LessThanNode::getValue()
{
    auto leftValue = left->getValue();
    auto rightValue = right->getValue();

    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(leftValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() < r->getValue()));
        }
        else
        {
            typeError("bool");
        }
    }

    typeError("bool");
}

GreaterThanNode::GreaterThanNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string GreaterThanNode::toString()
{
    return "GreaterThanNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> GreaterThanNode::getValue()
{
    auto leftValue = left->getValue();
    auto rightValue = right->getValue();

    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(leftValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() > r->getValue()));
        }
        else
        {
            typeError("bool");
        }
    }

    typeError("bool");
}

LessThanOrEqualNode::LessThanOrEqualNode(shared_ptr<Token> token,
                                         const shared_ptr<Node> &left,
                                         const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string LessThanOrEqualNode::toString()
{
    return "LessThanOrEqualNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> LessThanOrEqualNode::getValue()
{
    auto leftValue = left->getValue();
    auto rightValue = right->getValue();

    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(leftValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() <= r->getValue()));
        }
        else
        {
            typeError("bool");
        }
    }

    typeError("bool");
}

GreaterThanOrEqualNode::GreaterThanOrEqualNode(shared_ptr<Token> token,
                                               const shared_ptr<Node> &left,
                                               const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string GreaterThanOrEqualNode::toString()
{
    return "GreaterThanOrEqualNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> GreaterThanOrEqualNode::getValue()
{
    auto leftValue = left->getValue();
    auto rightValue = right->getValue();

    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(leftValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() >= r->getValue()));
        }
        else
        {
            typeError("bool");
        }
    }

    typeError("bool");
}

AssignmentNode::AssignmentNode(shared_ptr<Token> token,
                               const shared_ptr<Node> &varNode,
                               const shared_ptr<Node> &rValueNode)
        : Node(token), varNode(varNode), rValueNode(rValueNode)
{}

const shared_ptr<Node> &AssignmentNode::getVarNode() const
{
    return varNode;
}

const shared_ptr<Node> &AssignmentNode::getRValueNode() const
{
    return rValueNode;
}

string AssignmentNode::toString()
{
    return "AssignmentNode(var: " + varNode->toString() + ", value: " + rValueNode->toString() + ")";
}

shared_ptr<ValueType> AssignmentNode::getValue()
{
    auto result = rValueNode->getValue();
    if (auto _ = dynamic_pointer_cast<NoValue>(result))
    {
        typeError("string, int, array, bool");
    }

    if (auto var = dynamic_pointer_cast<VarNode>(varNode)) {
        auto varName = var->getVarName();

        table->set(varName, result);
    } else if (auto arr = dynamic_pointer_cast<IndexSetNode>(varNode)) {
        auto varName = arr->getVarNode()->getToken()->getLexeme();
        if (auto arrVar = dynamic_pointer_cast<ArrayValueType>(table->find(varName))) {
            if (auto index = dynamic_pointer_cast<IntValueType>(arr->getIndexNode()->getValue())) {
                arrVar->getValues()->at((unsigned long) index->getValue()) = result;
            } else {
                typeError("int");
            }
        } else {
            typeError("array");
        }
    }

    return make_shared<NoValue>(NoValue());
}

WhileNode::WhileNode(shared_ptr<Token> token, const shared_ptr<Node> &conditionNode, const shared_ptr<Node> &bodyNode)
        : Node(token), conditionNode(conditionNode), bodyNode(bodyNode)
{}

const shared_ptr<Node> &WhileNode::getConditionNode() const
{
    return conditionNode;
}

const shared_ptr<Node> &WhileNode::getBodyNode() const
{
    return bodyNode;
}

string WhileNode::toString()
{
    return "WhileNode(condition: " + conditionNode->toString() + ", body: " + bodyNode->toString() + ")";
}

shared_ptr<ValueType> WhileNode::getValue()
{
    while (true)
    {
        if (auto condition = dynamic_pointer_cast<BoolValueType>(conditionNode->getValue()))
        {
            if (condition->getValue())
            {
                bodyNode->getValue();
            }
            else
            {
                break;
            }
        }
        else
        {
            typeError("bool");
        }
    }

    return make_shared<NoValue>(NoValue());
}

string BlockNode::toString()
{
    return "BlockNode(statements: " + statementListNode->toString() + ")";
}

const shared_ptr<Node> &BlockNode::getStatementListNode() const
{
    return statementListNode;
}

BlockNode::BlockNode(shared_ptr<Token> token, const shared_ptr<Node> &statementListNode)
        : Node(token), statementListNode(statementListNode)
{}

shared_ptr<ValueType> BlockNode::getValue()
{
    return statementListNode->getValue();
}

StatementListNode::StatementListNode(shared_ptr<Token> token, const shared_ptr<vector<shared_ptr<Node>>> &statements)
        : Node(token), statements(statements)
{}

string StatementListNode::toString()
{
    string statementsStr;
    for (const auto &statement : *statements)
    {
        if (!statementsStr.empty())
        {
            statementsStr += ", ";
        }
        statementsStr += statement->toString();
    }
    return "StatementListNode(statements: " + statementsStr + ")";
}

const shared_ptr<vector<shared_ptr<Node>>> &StatementListNode::getStatements() const
{
    return statements;
}

shared_ptr<ValueType> StatementListNode::getValue()
{
    for (auto a : *statements)
    {
        a->getValue();
    }

    return make_shared<NoValue>(NoValue());
}

PrintArgListNode::PrintArgListNode(shared_ptr<Token> token, const shared_ptr<vector<shared_ptr<Node>>> &args)
        : Node(token), args(args)
{}

const shared_ptr<vector<shared_ptr<Node>>> &PrintArgListNode::getArgs() const
{
    return args;
}

string PrintArgListNode::toString()
{
    string argsStr;
    for (const auto &arg : *args)
    {
        if (!argsStr.empty())
        {
            argsStr += ", ";
        }
        argsStr += arg->toString();
    }
    return "PrintArgListNode(statements: " + argsStr + ")";
}

shared_ptr<ValueType> PrintArgListNode::getValue()
{
    std::string content;

    for (auto elem : *args)
    {
        if (!content.empty())
        {
            content += " ";
        }
        content += elem->getValue()->toString();
    }

    return make_shared<StringValueType>(StringValueType(content));
}

PrintNode::PrintNode(shared_ptr<Token> token, const shared_ptr<Node> &node) : Node(token), node(node)
{}

const shared_ptr<Node> &PrintNode::getNode() const
{
    return node;
}

string PrintNode::toString()
{
    return "PrintNode(args: " + node->toString() + ")";
}

shared_ptr<ValueType> PrintNode::getValue()
{
    auto value = node->getValue();
    if (auto _ = dynamic_pointer_cast<NoValue>(value))
    {
        typeError("int, bool, string, array");
    }

    std::cout << value->toString() << std::endl;
    return make_shared<NoValue>(NoValue());
}

IfNode::IfNode(shared_ptr<Token> token,
               const shared_ptr<Node> &conditionNode,
               const shared_ptr<Node> &ifBodyNode,
               const shared_ptr<Node> &elseBodyNode)
        : Node(token),
          conditionNode(conditionNode),
          ifBodyNode(ifBodyNode),
          elseBodyNode(elseBodyNode)
{}

const shared_ptr<Node> &IfNode::getConditionNode() const
{
    return conditionNode;
}

shared_ptr<ValueType> IfNode::getValue()
{
    auto conditionValue = conditionNode->getValue();
    if (auto booleanValue = dynamic_pointer_cast<BoolValueType>(conditionValue))
    {
        if (booleanValue->getValue())
        {
            ifBodyNode->getValue();
        }
        else
        {
            elseBodyNode->getValue();
        }
    }
    else
    {
        typeError("bool");
    }

    return make_shared<NoValue>(NoValue());
}

const shared_ptr<Node> &IfNode::getIfBodyNode() const
{
    return ifBodyNode;
}

const shared_ptr<Node> &IfNode::getElseBodyNode() const
{
    return elseBodyNode;
}

string IfNode::toString()
{
    return "IfStatement(condition: " + conditionNode->toString() + ", body: " + ifBodyNode->toString() +
           ", else: " + elseBodyNode->toString() + ")";
}

ForNode::ForNode(shared_ptr<Token> token,
                 const shared_ptr<Node> &varNode,
                 const shared_ptr<Node> &rangeNode,
                 const shared_ptr<Node> &body)
        : Node(token), varNode(varNode), rangeNode(rangeNode), body(body)
{}

const shared_ptr<Node> &ForNode::getVarNode() const
{
    return varNode;
}

const shared_ptr<Node> &ForNode::getRangeNode() const
{
    return rangeNode;
}

const shared_ptr<Node> &ForNode::getBody() const
{
    return body;
}

string ForNode::toString()
{
    return "ForNode(var: " + varNode->toString() + ", range: " + rangeNode->toString() +
           ", body: " + body->toString() + ")";
}

shared_ptr<ValueType> ForNode::getValue()
{
    auto variable = static_pointer_cast<VarNode>(varNode)->getVarName();
    auto range = static_pointer_cast<ArrayValueType>(rangeNode->getValue());

    for (const auto &i : *range->getValues())
    {
        table->set(variable, i);
        body->getValue();
    }

    return make_shared<NoValue>(NoValue());
}

RangeNode::RangeNode(shared_ptr<Token> token,
                     const shared_ptr<Node> &lowerBoundNode,
                     const shared_ptr<Node> &upperBoundNode)
        : Node(token),
          lowerBoundNode(lowerBoundNode),
          upperBoundNode(upperBoundNode)
{}

const shared_ptr<Node> &RangeNode::getLowerBoundNode() const
{
    return lowerBoundNode;
}

const shared_ptr<Node> &RangeNode::getUpperBoundNode() const
{
    return upperBoundNode;
}

string RangeNode::toString()
{
    return "RangeNode(lower: " + lowerBoundNode->toString() + ", upper: " + upperBoundNode->toString() + ")";
}

shared_ptr<ValueType> RangeNode::getValue()
{
    if (auto lowerConst = dynamic_pointer_cast<IntValueType>(lowerBoundNode->getValue()))
    {
        if (auto upperConst = dynamic_pointer_cast<IntValueType>(upperBoundNode->getValue()))
        {
            auto rangeVector = make_shared<vector<shared_ptr<ValueType>>>();
            for (int i = lowerConst->getValue(); i < upperConst->getValue(); i++)
            {
                rangeVector->push_back(make_shared<IntValueType>(i));
            }
            return make_shared<ArrayValueType>(ArrayValueType(rangeVector));
        }
    }

    typeError("int");
}

ArrayNode::ArrayNode(shared_ptr<Token> token, const shared_ptr<vector<shared_ptr<Node>>> &elems)
        : Node(token), elems(elems)
{}

const shared_ptr<vector<shared_ptr<Node>>> &ArrayNode::getElems() const
{
    return elems;
}

string ArrayNode::toString()
{
    string argsStr;
    for (const auto &arg : *elems)
    {
        if (!argsStr.empty())
        {
            argsStr += ", ";
        }
        argsStr += arg->toString();
    }
    return "ArrayNode(children: " + argsStr + ")";
}


shared_ptr<ValueType> ArrayNode::getValue()
{
    auto arrayValues = make_shared<vector<shared_ptr<ValueType>>>();
    for (const auto &elem : *elems)
    {
        auto value = elem->getValue();
        if (auto _ = dynamic_pointer_cast<NoValue>(value))
        {
            typeError("bool, array, string, int");
        }
        else
        {
            arrayValues->push_back(value);
        }
    }

    return make_shared<ArrayValueType>(ArrayValueType(arrayValues));
}

LenNode::LenNode(shared_ptr<Token> token, const shared_ptr<Node> &contentNode) : Node(token), contentNode(contentNode)
{}

const shared_ptr<Node> &LenNode::getContentNode() const
{
    return contentNode;
}

string LenNode::toString()
{
    return "LenNode(content: " + contentNode->toString() + ")";
}

shared_ptr<ValueType> LenNode::getValue()
{
    if (auto array = dynamic_pointer_cast<ArrayValueType>(contentNode->getValue()))
    {
        return make_shared<IntValueType>(IntValueType(static_cast<int>(array->getValues()->size())));
    }
    else
    {
        typeError("array");
    }
}

IndexingNode::IndexingNode(shared_ptr<Token> token, const shared_ptr<Node> &varNode, const shared_ptr<Node> &indexNode)
        : Node(token), varNode(varNode), indexNode(indexNode)
{}

const shared_ptr<Node> &IndexingNode::getVarNode() const
{
    return varNode;
}

const shared_ptr<Node> &IndexingNode::getIndexNode() const
{
    return indexNode;
}

string IndexingNode::toString()
{
    return "IndexNode(var: " + varNode->toString() + ", index: " + indexNode->toString() + ")";
}

IndexAccessNode::IndexAccessNode(shared_ptr<Token> token,
                                 const shared_ptr<Node> &varNode,
                                 const shared_ptr<Node> &indexNode)
        : IndexingNode(token, varNode, indexNode)
{}

IndexSetNode::IndexSetNode(shared_ptr<Token> token, const shared_ptr<Node> &varNode, const shared_ptr<Node> &indexNode)
        : IndexingNode(token, varNode, indexNode)
{}

shared_ptr<ValueType> IndexSetNode::getValue() {
    auto var = table->find(getVarNode()->getToken()->getLexeme());
    if (auto arrVar = dynamic_pointer_cast<ArrayValueType>(var)) {
        if(auto index = dynamic_pointer_cast<IntValueType>(getIndexNode()->getValue())) {
            return arrVar->getValues()->at((unsigned long) index->getValue());
        } else {
            typeError("int");
        }
    } else {
        typeError("array");
    }
}

NotEqualNode::NotEqualNode(shared_ptr<Token> token, const shared_ptr<Node> &left, const shared_ptr<Node> &right)
        : BinaryOpNode(token, left, right)
{}

string NotEqualNode::toString()
{
    return "NotEqualNode(left: " + left->toString() + ", right: " + right->toString() + ")";
}

shared_ptr<ValueType> NotEqualNode::getValue()
{
    auto leftValue = left->getValue();
    auto rightValue = right->getValue();

    if (shared_ptr<IntValueType> l = dynamic_pointer_cast<IntValueType>(leftValue))
    {
        if (shared_ptr<IntValueType> r = dynamic_pointer_cast<IntValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() != r->getValue()));
        }
        else
        {
            typeError("int");
        }
    }
    else if (shared_ptr<StringValueType> l = dynamic_pointer_cast<StringValueType>(leftValue))
    {
        if (shared_ptr<StringValueType> r = dynamic_pointer_cast<StringValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() != r->getValue()));
        }
        else
        {
            typeError("string");
        }
    }
    else if (shared_ptr<BoolValueType> l = dynamic_pointer_cast<BoolValueType>(leftValue))
    {
        if (shared_ptr<BoolValueType> r = dynamic_pointer_cast<BoolValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(l->getValue() != r->getValue()));
        }
        else
        {
            typeError("bool");
        }
    }
    else if (auto l = dynamic_pointer_cast<ArrayValueType>(leftValue))
    {
        if (auto r = dynamic_pointer_cast<ArrayValueType>(rightValue))
        {
            return make_shared<BoolValueType>(BoolValueType(*l->getValues() == *r->getValues()));
        }
        else
        {
            typeError("array");
        }
    }

    typeError("int, string, bool, array");
}

shared_ptr<ValueType> Node::getValue()
{
    return make_shared<NoValue>(NoValue());
}

Node::Node(const shared_ptr<token::Token> &token) : token(token)
{}

const shared_ptr<token::Token> &Node::getToken() const
{
    return token;
}
