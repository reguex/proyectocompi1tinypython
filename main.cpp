#include <iostream>
#include "lexer/Lexer.h"
#include "parser/Parser.h"
#include "ast/VariableTable.h"

using namespace std;
using namespace ast;

int main(int argc, char *argv[]) {

    std::string filePath;
    if (argc == 2) {
        filePath = argv[1];
    } else {
        cout << "File: ";
        cin >> filePath;
    }

    std::ifstream ifs;
    ifs.open(filePath);

    lexer::Lexer lexer(ifs);
    parser::Parser parser(lexer);

    auto tree = parser.buildAST();
    tree->getValue();

    ifs.close();
}