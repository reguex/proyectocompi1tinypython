cmake_minimum_required(VERSION 3.7)
project(ProyectoCompi1TinyPython)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_FLAGS "-pedantic -std=c++11 -Wall -Wextra -Wformat-nonliteral -Wcast-align -Wpointer-arith -Wbad-function-cast -Wmissing-prototypes -Wstrict-prototypes -Wmissing-declarations -Winline -Wundef -Wnested-externs -Wcast-qual -Wwrite-strings -Wno-unused-parameter -Wfloat-equal")

set(SOURCE_FILES main.cpp token/TokenType.cpp token/TokenType.h token/Token.cpp token/Token.h lexer/Lexer.cpp lexer/Lexer.h chars/chars.cpp chars/chars.h lexer/_SpaceHandler.cpp lexer/_SpaceHandler.h lexer/_IndentResult.cpp lexer/_IndentResult.h parser/Parser.cpp parser/Parser.h ast/Node.cpp ast/Node.h ast/ValueType.cpp ast/ValueType.h ast/VariableTable.cpp ast/VariableTable.h)
add_executable(TinyPython ${SOURCE_FILES})