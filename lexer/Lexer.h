//
// Created by Doninelli on 12/2/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_LEXER_H
#define PROYECTOCOMPI1TINYPYTHON_LEXER_H


#include <fstream>
#include <memory>

#include "../token/Token.h"
#include "_SpaceHandler.h"

namespace lexer
{
    class Lexer
    {
        std::ifstream &input;
        int line;

        bool previousWasNewline;

        _SpaceHandler spaceHandler;

    public:
        explicit Lexer(std::ifstream &input);

        bool isComplete();

        std::shared_ptr<token::Token> scan();

    private:
        [[noreturn]] void error(const std::string &message);

    private:
        char getCurrentChar();

        void advance();

        std::shared_ptr<token::Token> scanDigit();

        std::shared_ptr<token::Token> scanId();

        std::shared_ptr<token::Token> scanSingleQuoteString();

        std::shared_ptr<token::Token> scanDoubleQuotesString();

        void consumeNewLine();

        token::TokenType scanSpace();

        std::shared_ptr<token::Token> scanSingleChar(token::TokenType type);

        std::shared_ptr<token::Token> scanAsterisk();

        std::shared_ptr<token::Token> scanEqual();

        std::shared_ptr<token::Token> scanLessThan();

        std::shared_ptr<token::Token> scanGreaterThan();

        void skipComment();

        std::shared_ptr<token::Token> scanExclamation();
    };
}


#endif //PROYECTOCOMPI1TINYPYTHON_LEXER_H
