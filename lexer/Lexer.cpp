//
// Created by Doninelli on 12/2/17.
//

#include "Lexer.h"
#include "../chars/chars.h"

lexer::Lexer::Lexer(std::ifstream &input) : input(input), line(1), previousWasNewline(false), spaceHandler()
{}

char lexer::Lexer::getCurrentChar()
{
    int currentChar;
    if (!isComplete())
    {
        currentChar = input.peek();
    }
    else
    {
        currentChar = EOF;
    }
    return static_cast<char>(currentChar);
}

bool lexer::Lexer::isComplete()
{
    return input.eof() || input.peek() == EOF;
}

void lexer::Lexer::advance()
{
    if (!isComplete())
    {
        input.get();
    }
}

std::shared_ptr<token::Token> lexer::Lexer::scan()
{
    while (!isComplete())
    {
        char currentChar = getCurrentChar();

        std::shared_ptr<token::Token> token;
        if (chars::isNewLine(currentChar))
        {
            consumeNewLine();
            continue;
        }
        else if (currentChar == '#')
        {
            skipComment();
            continue;
        }
        else if (chars::isSpace(currentChar) || previousWasNewline)
        {
            token::TokenType tokenType = scanSpace();
            if (tokenType == token::TokenType::SPACE)
            {
                continue;
            }
            else
            {
                token = token::newShared("", tokenType, line);
            }
        }
        else if (chars::isDigit(currentChar))
        {
            previousWasNewline = false;
            token = scanDigit();
        }
        else if (chars::isLetter(currentChar))
        {
            previousWasNewline = false;
            token = scanId();
        }
        else
        {
            previousWasNewline = false;
            switch (currentChar)
            {
                case '\'':
                    token = scanSingleQuoteString();
                    break;

                case '\"':
                    token = scanDoubleQuotesString();
                    break;

                case '+':
                    token = scanSingleChar(token::TokenType::ADD);
                    break;

                case '-':
                    token = scanSingleChar(token::TokenType::SUB);
                    break;

                case '*':
                    token = scanAsterisk();
                    break;

                case '/':
                    token = scanSingleChar(token::TokenType::DIV);
                    break;

                case '%':
                    token = scanSingleChar(token::TokenType::MOD);
                    break;

                case '=':
                    token = scanEqual();
                    break;

                case '<':
                    token = scanLessThan();
                    break;

                case '>':
                    token = scanGreaterThan();
                    break;

                case ':':
                    token = scanSingleChar(token::TokenType::COLON);
                    break;

                case '(':
                    token = scanSingleChar(token::TokenType::L_PAREN);
                    break;

                case ')':
                    token = scanSingleChar(token::TokenType::R_PAREN);
                    break;

                case ',':
                    token = scanSingleChar(token::TokenType::COMMA);
                    break;

                case '[':
                    token = scanSingleChar(token::TokenType::L_BRACKET);
                    break;

                case ']':
                    token = scanSingleChar(token::TokenType::R_BRACKET);
                    break;

                case '!':
                    token = scanExclamation();
                    break;

                default:
                    token = nullptr;
                    break;
            }
        }

        if (token != nullptr)
        {
            return token;
        }
        else
        {
            std::string message = "Invalid Character '";
            message += currentChar;
            message += "'";
            error(message);
        }
    }

    return token::newShared("", token::TokenType::END_OF_FILE, line);
}

std::shared_ptr<token::Token> lexer::Lexer::scanDigit()
{
    std::string lexeme;
    while (chars::isDigit(getCurrentChar()))
    {
        lexeme += getCurrentChar();
        advance();
    }
    return token::newShared(lexeme, token::TokenType::INT, line);
}

std::shared_ptr<token::Token> lexer::Lexer::scanId()
{
    std::string lexeme;
    if (chars::isLetter(getCurrentChar()) || getCurrentChar() == '_')
    {
        lexeme += getCurrentChar();
        advance();
    }

    while (chars::isLetter(getCurrentChar()) || chars::isDigit(getCurrentChar()) || getCurrentChar() == '_')
    {
        lexeme += getCurrentChar();
        advance();
    }

    token::TokenType type;
    if (lexeme == "while")
    {
        type = token::TokenType::WHILE;
    }
    else if (lexeme == "if")
    {
        type = token::TokenType::IF;
    }
    else if (lexeme == "else")
    {
        type = token::TokenType::ELSE;
    }
    else if (lexeme == "for")
    {
        type = token::TokenType::FOR;
    }
    else if (lexeme == "in")
    {
        type = token::TokenType::IN;
    }
    else if (lexeme == "range")
    {
        type = token::TokenType::RANGE;
    }
    else if (lexeme == "pass")
    {
        type = token::TokenType::PASS;
    }
    else if (lexeme == "input")
    {
        type = token::TokenType::INPUT;
    }
    else if (lexeme == "print")
    {
        type = token::TokenType::PRINT;
    }
    else if (lexeme == "len")
    {
        type = token::TokenType::LEN;
    }
    else
    {
        type = token::TokenType::ID;
    }

    return token::newShared(lexeme, type, line);
}

std::shared_ptr<token::Token> lexer::Lexer::scanSingleQuoteString()
{
    std::string lexeme;
    lexeme += getCurrentChar();
    advance();

    while (!isComplete() && getCurrentChar() != '\'')
    {
        lexeme += getCurrentChar();
        advance();
    }

    if (getCurrentChar() != '\'')
    {
        error("Unclosed string");
    }
    else
    {
        lexeme += getCurrentChar();
        advance();
    }

    return token::newShared(lexeme, token::TokenType::STRING, line);
}

void lexer::Lexer::error(const std::string &message)
{
    throw std::invalid_argument(message + " at line: " + std::to_string(line));
}

std::shared_ptr<token::Token> lexer::Lexer::scanDoubleQuotesString()
{
    std::string lexeme;
    lexeme += getCurrentChar();
    advance();

    while (!isComplete() && getCurrentChar() != '\"')
    {
        lexeme += getCurrentChar();
        advance();
    }

    if (getCurrentChar() != '\"')
    {
        error("Unclosed string");
    }
    else
    {
        lexeme += getCurrentChar();
        advance();
    }

    return token::newShared(lexeme, token::TokenType::STRING, line);
}

void lexer::Lexer::consumeNewLine()
{
    line += 1;
    previousWasNewline = true;
    advance();
}

token::TokenType lexer::Lexer::scanSpace()
{
    token::TokenType tokenType;
    if (previousWasNewline)
    {
        auto result = spaceHandler.scanPossibleIndentations(input);
        tokenType = result.getTokenType();
        previousWasNewline = result.isMoreSpaceAfterDedent();
    }
    else
    {
        advance();
        tokenType = token::TokenType::SPACE;
    }
    return tokenType;
}

std::shared_ptr<token::Token> lexer::Lexer::scanSingleChar(token::TokenType type)
{
    std::string lexeme;
    lexeme += getCurrentChar();

    advance();
    return token::newShared(lexeme, type, line);
}

std::shared_ptr<token::Token> lexer::Lexer::scanAsterisk()
{
    std::string lexeme;
    lexeme += getCurrentChar();

    advance();

    token::TokenType type;
    if (getCurrentChar() == '*')
    {
        type = token::TokenType::EXP;
        lexeme += getCurrentChar();
        advance();
    }
    else
    {
        type = token::TokenType::MUL;
    }

    return token::newShared(lexeme, type, line);

}

std::shared_ptr<token::Token> lexer::Lexer::scanEqual()
{
    std::string lexeme;
    lexeme += getCurrentChar();
    advance();

    token::TokenType type;
    if (getCurrentChar() == '=')
    {
        lexeme += getCurrentChar();
        advance();
        type = token::TokenType::EQUAL;
    }
    else
    {
        type = token::TokenType::ASSIGN;
    }

    return token::newShared(lexeme, type, line);
}

std::shared_ptr<token::Token> lexer::Lexer::scanLessThan()
{
    std::string lexeme;
    lexeme += getCurrentChar();
    advance();

    token::TokenType type;
    if (getCurrentChar() == '=')
    {
        lexeme += getCurrentChar();
        advance();
        type = token::TokenType::LESS_THAN_OR_EQ;
    }
    else
    {
        type = token::TokenType::LESS_THAN;
    }

    return token::newShared(lexeme, type, line);
}

std::shared_ptr<token::Token> lexer::Lexer::scanGreaterThan()
{
    std::string lexeme;
    lexeme += getCurrentChar();
    advance();

    token::TokenType type;
    if (getCurrentChar() == '=')
    {
        lexeme += getCurrentChar();
        advance();
        type = token::TokenType::GREATER_THAN_OR_EQ;
    }
    else
    {
        type = token::TokenType::GREATER_THAN;
    }

    return token::newShared(lexeme, type, line);
}

void lexer::Lexer::skipComment()
{
    while (!isComplete() && getCurrentChar() != '\n')
    {
        advance();
    }
}

std::shared_ptr<token::Token> lexer::Lexer::scanExclamation()
{
    std::string lexeme;
    lexeme += getCurrentChar();
    advance();

    if (getCurrentChar() != '=') {
        error("Invalid Input. " + std::to_string(getCurrentChar()));
    }

    lexeme += getCurrentChar();
    advance();

    return token::newShared(lexeme, token::TokenType::NOT_EQUALS, line);
}
