//
// Created by Doninelli on 12/2/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_INDENTRESULT_H
#define PROYECTOCOMPI1TINYPYTHON_INDENTRESULT_H


#include "../token/TokenType.h"

namespace lexer
{
    class _IndentResult
    {
        token::TokenType tokenType;
        bool moreSpaceAfterDedent;

    public:
        _IndentResult(token::TokenType tokenType, bool moreSpaceAfterDedent);

        token::TokenType getTokenType() const;

        bool isMoreSpaceAfterDedent() const;
    };
}


#endif //PROYECTOCOMPI1TINYPYTHON_INDENTRESULT_H
