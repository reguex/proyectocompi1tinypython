//
// Created by Doninelli on 12/2/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_INDENTHANDLER_H
#define PROYECTOCOMPI1TINYPYTHON_INDENTHANDLER_H


#include <stack>
#include <fstream>
#include <vector>
#include "../token/Token.h"
#include "_IndentResult.h"

namespace lexer
{
    class _SpaceHandler
    {
    private:
        std::stack<int> indentLevels;

    public:
        _SpaceHandler();

        _IndentResult scanPossibleIndentations(std::ifstream &source);
    };


}


#endif //PROYECTOCOMPI1TINYPYTHON_INDENTHANDLER_H
