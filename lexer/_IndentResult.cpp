//
// Created by Doninelli on 12/2/17.
//

#include "_IndentResult.h"

lexer::_IndentResult::_IndentResult(token::TokenType tokenType, bool moreSpaceAfterDedent) :
        tokenType(tokenType),
        moreSpaceAfterDedent(moreSpaceAfterDedent)
{}

token::TokenType lexer::_IndentResult::getTokenType() const
{
    return tokenType;
}

bool lexer::_IndentResult::isMoreSpaceAfterDedent() const
{
    return moreSpaceAfterDedent;
}