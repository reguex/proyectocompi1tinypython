//
// Created by Doninelli on 12/2/17.
//

#include "_SpaceHandler.h"
#include "../chars/chars.h"

lexer::_SpaceHandler::_SpaceHandler()
{
    indentLevels = std::stack<int>();
    indentLevels.push(0);
}

lexer::_IndentResult lexer::_SpaceHandler::scanPossibleIndentations(std::ifstream &source)
{
    std::string lexeme;
    while (chars::isSpace(static_cast<char>(source.peek())))
    {
        lexeme += static_cast<char>(source.get());
    }

    auto lexemeLength = static_cast<int>(lexeme.length());
    auto lastIndent = indentLevels.top();

    token::TokenType tokenType;
    bool dedentRemaining = false;
    if (lastIndent == lexemeLength)
    {
        tokenType = token::TokenType::SPACE;
    }
    else if (lexemeLength > lastIndent)
    {
        indentLevels.push(lexemeLength);
        tokenType = token::TokenType::INDENT;
    }
    else
    {
        indentLevels.pop();
        tokenType = token::TokenType::DEDENT;

        dedentRemaining = true;
        if (lexemeLength != 0)
        {
            for (int i = 0; i <= lexemeLength; i++)
            {
                source.unget();
            }
        }
    }

    return {tokenType, dedentRemaining};
}
