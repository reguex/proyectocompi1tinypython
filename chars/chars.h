//
// Created by Doninelli on 12/2/17.
//

#ifndef PROYECTOCOMPI1TINYPYTHON_CHARS_H
#define PROYECTOCOMPI1TINYPYTHON_CHARS_H


#include <cctype>
#include <iostream>

namespace chars {

    inline int toInt(char c) {
        int c_int = c - '0';
        return c_int;
    }

    inline bool isSpace(char c) {
        return isspace(c);
    }

    inline bool isNewLine(char c) {
        return c == '\n';
    }

    inline bool isDigit(char c) {
        return isdigit(c);
    }

    inline bool isLetter(char c) {
        return isalpha(c) && !isDigit(c);
    }
}


#endif //PROYECTOCOMPI1TINYPYTHON_CHARS_H
